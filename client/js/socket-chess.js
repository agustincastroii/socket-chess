/** socket-chess.js
 * Client for SocketChess, a Node.js and Socket.io driven
 * online chess game.
 * 
 * Author: Agustin Castro <castro.agustin.ii@gmail.com>
 */

(function(){
	'use strict';

	window.onload = function(){
		new SocketChess('app');
	};
 	
 	function SocketChess (id, options){
 		if (this instanceof SocketChess) {

 			this.options = extend({}, SocketChess.DEFAULTS, options);
 			this.dom = document.getElementById(id);

			this.render();

		}
		else {
			return new SocketChess(id, options);
		}
 	}
 	
 	SocketChess.DEFAULTS = {
 		playerColor: 'white',
 		blackPiece: '#333',
 		whitePiece: '#ddd',
 		blackTile: '#000',
 		whiteTile: '#fff',
 		tileSize: 50
 	};

 	SocketChess.START_POSITIONS_WHITE = ['r', 'h', 'b', 'q', 'k', 'b', 'h', 'r'];

 	SocketChess.START_POSITIONS_BLACK = ['t', 'j', 'n', 'w', 'l', 'n', 'j', 't'];

 	SocketChess.prototype.render = function(){
 		/*
 		// render board
 		var board = document.createElement('canvas');
 		board.width = 400;
 		board.height = 400;
 		board.setAttribute('class', 'board');
 		var boardContext = board.getContext('2d');
 		this.dom.appendChild(board);
 		
 		// fill tiles with alternating colors
 		// black and white tile hex value specified in options
 		for (var row = 0; row < 8; row++) {
 			for (var col = 0; col < 8; col++) {

 				// coordinates of top-left corner of tile
 				var x = col * this.options.tileSize;
 				var y = row * this.options.tileSize;

 				// alternate colors
 				if (row % 2 === 0) {
 					if (col % 2 === 0) {
 						boardContext.fillStyle = this.options.blackTile;
 					}
 					else {
 						boardContext.fillStyle = this.options.whiteTile;	
 					}
 				}
 				else {
 					if (col % 2 === 0) {
 						boardContext.fillStyle = this.options.whiteTile;
 					}
 					else {
 						boardContext.fillStyle = this.options.blackTile;
 					}
 				}

 				boardContext.fillRect(x, y, this.options.tileSize, this.options.tileSize);
 			}
 		}
 		*/
 		// insert new canvas for game pieces
 		var pieces = document.createElement('canvas');
 		pieces.width = 400;
 		pieces.height = 400;
 		pieces.setAttribute('class', 'pieces');
 		var piecesContext = pieces.getContext('2d');
 		piecesContext.font = '50px Cheq';
 		piecesContext.textBaseline = 'top';
 		this.dom.appendChild(pieces);

 		if (this.options.playerColor == 'white') {
 			var y = 0;
 		}
 		else {
 			var y = 350;
 		}
 		// render pieces at start positions using CHEQ_TT font
 		for (var col = 0; col < 8; col++) {

 			// coordinates of top-left corner of tile
 			var x = col * this.options.tileSize;

 			if (this.options.playerColor == 'white') {
 				piecesContext.fillStyle = this.options.whitePiece;
				piecesContext.fillText(SocketChess.START_POSITIONS_WHITE[col], x, y);
			}	
			else {
 				piecesContext.fillStyle = this.options.blackPiece;
				piecesContext.fillText(SocketChess.START_POSITIONS_BLACK[col], x, y);
			}

 		}

 		/* CHEQ Font Legend:
 		 * White:
 		 *		Pawn: p
 		 *		Rook: r
 		 *		Knight: h
 		 *		Bishop: b
 		 *		Queen: q
 		 *		King: k
 		 * Black:
 		 *		Pawn: o
 		 *		Rook: t
 		 *		Knight: j
 		 *		Bishop: n
 		 *		Queen: w
 		 *		King: l
 		 **/
 		 


 	};


 	// Helper functions
 	function extend(o){
	    for(var i = 1, i_len = arguments.length; i < i_len; i++) {
	        var arg = arguments[i];
	        if(arg){
	            for(var name in arg){
	                o[name] = arg[name];
	            }
	        }
	    }
	    return o;
	}

})(document);

